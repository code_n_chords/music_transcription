import librosa
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader, TensorDataset
from midiutil import MIDIFile
from miditoolkit import MidiFile
import subprocess
import music21 as m21
import matplotlib.pyplot as plt
import librosa.display
from pydub import AudioSegment
from pydub.playback import play

# Load the audio file
filename = "audio_music_Guns%20N%20Roses-Sweet%20Child%20O%20Mine%20Intro.mp3"
y, sr = librosa.load(filename, sr=None, mono=True, duration=4)

# Constants for CQT calculation
hop_length = 512
n_bins = 84
mag_exp = 1
cqt_threshold = -30

# Function to calculate CQT
def calc_cqt(x, fs, hop_length, n_bins, mag_exp):
    c = librosa.cqt(x, sr=fs, hop_length=hop_length, n_bins=n_bins)
    c_mag = np.abs(c) ** mag_exp
    CdB = librosa.amplitude_to_db(c_mag, ref=np.max)
    return CdB

# Function to threshold CQT
def cqt_thresholded(cqt, thres=cqt_threshold):
    new_cqt = np.copy(cqt)
    new_cqt[new_cqt < thres] = -120
    return new_cqt

# Calculate CQT and threshold it
cqt_db = calc_cqt(y, sr, hop_length, n_bins, mag_exp)
thresholded_cqt = cqt_thresholded(cqt_db)

# Display audio information
print("y shape= ", y.shape)
print("sample rate sr= ", sr)
print("audio length in seconds= ", len(y) / sr)

# Convert audio data to pydub's AudioSegment format
audio_segment = AudioSegment(
    data=y.tobytes(),
    sample_width=y.dtype.itemsize,
    frame_rate=sr,
    channels=1
)

play(audio_segment)

def add_noise(y, noise_factor=0.005):
    noise = np.random.randn(len(y))
    augmented_data = y + noise_factor * noise
    return augmented_data

def time_stretch(y, rate=1.1):
    return librosa.effects.time_stretch(y, rate)

# Tempo estimation
tempo, _ = librosa.beat.beat_track(y=y, sr=sr)
print(f"Estimated Tempo: {tempo} BPM")

# Pitch estimation using harmonic-percussive source separation
harmonic = librosa.effects.harmonic(y=y, margin=8)
pitches, magnitudes = librosa.core.piptrack(y=harmonic, sr=sr, hop_length=hop_length)
pitches = np.where(pitches > 0, pitches, 0)
magnitudes = np.where(magnitudes > 0, magnitudes, 0)

# Visualize pitches and magnitudes
plt.figure(figsize=(10, 4))
plt.subplot(2, 1, 1)
plt.plot(pitches.T)
plt.title('Estimated pitches over time')
plt.xlabel('Time frames')
plt.ylabel('Pitch')

plt.subplot(2, 1, 2)
plt.plot(magnitudes.T)
plt.title('Magnitude of pitches over time')
plt.xlabel('Time frames')
plt.ylabel('Magnitude')
plt.tight_layout()
plt.show()

# Display the CQT spectrogram
plt.figure(figsize=(10, 4))
librosa.display.specshow(thresholded_cqt, sr=sr, x_axis='time', y_axis='cqt_note')
plt.colorbar(format='%+2.0f dB')
plt.title('Constant-Q power spectrogram')
plt.tight_layout()
plt.show()

# Onset detection
onset_env = librosa.onset.onset_strength(y=y, sr=sr, hop_length=hop_length)
onsets = librosa.onset.onset_detect(onset_envelope=onset_env, sr=sr, hop_length=hop_length)
onset_times = librosa.frames_to_time(onsets, sr=sr, hop_length=hop_length)
print("Onset times: ", onset_times)

# Function to generate labels based on pitch detection
def generate_labels(pitches, magnitudes, sr, hop_length, num_classes):
    num_frames = pitches.shape[1]
    labels = np.zeros((1, num_frames, num_classes))

    for t in range(num_frames):
        pitch_col = pitches[:, t]
        mag_col = magnitudes[:, t]

        # Determine active pitches based on magnitude threshold
        active_pitches = np.where(mag_col > np.median(mag_col))[0]

        # Assign labels for active pitches
        for pitch_idx in active_pitches:
            pitch_value = pitch_col[pitch_idx]
            if pitch_value > 0:  # Only consider non-zero pitches
                pitch_class = min(int(round(pitch_value)), num_classes - 1)  # Round pitch for better matching
                labels[0, t, pitch_class] = 1

    return labels

# Generate labels for training and validation
num_classes = 88  
y_train = generate_labels(pitches, magnitudes, sr, hop_length, num_classes)
y_val = generate_labels(pitches, magnitudes, sr, hop_length, num_classes)

# Preparing data for model training
X = thresholded_cqt.T
X = np.expand_dims(X, axis=0)

# Convert data to PyTorch tensors
X_train = torch.tensor(X, dtype=torch.float32)
y_train = torch.tensor(y_train, dtype=torch.float32)
X_val = torch.tensor(X, dtype=torch.float32)
y_val = torch.tensor(y_val, dtype=torch.float32)

# Create a PyTorch dataset and dataloader
train_dataset = TensorDataset(X_train, y_train)
train_loader = DataLoader(train_dataset, batch_size=40, shuffle=True)
val_dataset = TensorDataset(X_val, y_val)
val_loader = DataLoader(val_dataset, batch_size=40)

# Define the Positional Encoding class
class PositionalEncoding(nn.Module):
    def __init__(self, d_model, max_len=5000):
        super(PositionalEncoding, self).__init__()
        self.encoding = torch.zeros(max_len, d_model)
        position = torch.arange(0, max_len).unsqueeze(1).float()
        div_term = torch.exp(torch.arange(0, d_model, 2).float() * -(np.log(10000.0) / d_model))
        self.encoding[:, 0::2] = torch.sin(position * div_term)
        self.encoding[:, 1::2] = torch.cos(position * div_term)
        self.encoding = self.encoding.unsqueeze(0)
    
    def forward(self, x):
        return x + self.encoding[:, :x.size(1)]

# Define the Transformer model in PyTorch
class TransformerModel(nn.Module):
    def __init__(self, input_size, num_heads, num_encoder_layers, num_decoder_layers, hidden_dim, output_size, dropout=0.1):
        super(TransformerModel, self).__init__()
        self.input_size = input_size
        self.hidden_dim = hidden_dim
        self.output_size = output_size

        self.encoder = nn.Linear(input_size, hidden_dim)
        self.positional_encoding = PositionalEncoding(d_model=hidden_dim)
        self.transformer = nn.Transformer(
            d_model=hidden_dim,
            nhead=num_heads,
            num_encoder_layers=num_encoder_layers,
            num_decoder_layers=num_decoder_layers,
            batch_first=True
        )
        self.dropout = nn.Dropout(dropout)
        self.decoder = nn.Linear(hidden_dim, output_size)
        self.sigmoid = nn.Sigmoid()
    
    def forward(self, src, tgt):
        src = self.encoder(src)
        tgt = self.encoder(tgt)
        src = self.positional_encoding(src)
        tgt = self.positional_encoding(tgt)

        src = self.dropout(src)
        tgt = self.dropout(tgt)

        output = self.transformer(src, tgt)
        output = self.dropout(output)
        output = self.decoder(output)
        output = self.sigmoid(output)
        return output

# Instantiate the model
model = TransformerModel(input_size=n_bins, num_heads=8, num_encoder_layers=6, num_decoder_layers=6, hidden_dim=512, output_size=num_classes)

# Loss and optimizer
criterion = nn.BCELoss()
optimizer = optim.Adam(model.parameters(), lr=0.001)

# Training the model
num_epochs = 200
for epoch in range(num_epochs):
    model.train()
    for i, (inputs, labels) in enumerate(train_loader):
        optimizer.zero_grad()
        outputs = model(inputs, inputs)  # Using inputs as both source and target
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()
    
    model.eval()
    val_loss = 0
    with torch.no_grad():
        for inputs, labels in val_loader:
            outputs = model(inputs, inputs)
            loss = criterion(outputs, labels)
            val_loss += loss.item()
    
    val_loss /= len(val_loader)
    print(f'Epoch [{epoch+1}/{num_epochs}], Loss: {loss.item()}, Val Loss: {val_loss}')

# Predict on new audio features
X_new = calc_cqt(y, sr, hop_length, n_bins, mag_exp).T  
X_new = np.expand_dims(X_new, axis=0)
X_new = torch.tensor(X_new, dtype=torch.float32)
model.eval()
predictions = model(X_new, X_new).detach().numpy()  # Using X_new as both source and target

# Convert predictions to a MIDI file
midi = MIDIFile(1)  # Create a single track MIDI file
midi.addTempo(0, 0, tempo)

# Add notes to the MIDI file based on the predictions
for i, pitch in enumerate(predictions[0]):
    if np.max(pitch) > 0.5:  
        midi.addNote(0, 0, int(np.argmax(pitch)), i * hop_length / sr, 1, 100)

# Save the MIDI file
output_midi_path = "output.mid"
with open(output_midi_path, "wb") as output_file:
    midi.writeFile(output_file)

# Load the MIDI file
midi_data = MidiFile(output_midi_path)

# Extract notes information
notes = []
for track in midi_data.instruments:
    for note in track.notes:
        notes.append((note.start, note.end, note.pitch, note.velocity))

# Print notes information
print("Notes Transcribed:")
for note in notes:
    print(f"Note: {note[2]}, Start: {note[0]}, End: {note[1]}, Velocity: {note[3]}")

# Set the path to MuseScore executable
musescore_path = '/Applications/MuseScore 4.app/Contents/MacOS/mscore'  # Update this path as needed
m21.environment.set('musicxmlPath', musescore_path)

# Load the MIDI file into music21
midi_score = m21.converter.parse(output_midi_path)

# Convert MIDI to MusicXML
musicxml_path = "output.musicxml"
midi_score.write('musicxml', fp=musicxml_path)

# Open the MusicXML file in MuseScore
subprocess.run([musescore_path, musicxml_path])

































