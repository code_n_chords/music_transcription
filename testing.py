import os
import librosa
import numpy as np
import matplotlib.pyplot as plt
import librosa.display
from tensorflow.keras.models import load_model
from midiutil import MIDIFile
from miditoolkit import MidiFile
import subprocess
import music21 as m21

# Constants for CQT calculation
hop_length = 512
n_bins = 84
mag_exp = 1
cqt_threshold = -30
num_classes = 88  # Adjust based on your task

# Function to calculate CQT
def calc_cqt(x, fs, hop_length, n_bins, mag_exp):
    c = librosa.cqt(x, sr=fs, hop_length=hop_length, n_bins=n_bins)
    c_mag = np.abs(c) ** mag_exp
    CdB = librosa.amplitude_to_db(c_mag, ref=np.max)
    return CdB

# Function to threshold CQT
def cqt_thresholded(cqt, thres=cqt_threshold):
    new_cqt = np.copy(cqt)
    new_cqt[new_cqt < thres] = -120
    return new_cqt

# Use the absolute path to the model file
model_path = "/Users/ramaniatluri/music_transcription-2/final_model.keras"
model = load_model(model_path)

# File to predict on
filename = "/Users/ramaniatluri/music_transcription-2/audio_music_Guns%20N%20Roses-Sweet%20Child%20O%20Mine%20Intro.mp3"

try:
    print(f"Processing file: {filename}")
    
    # Load audio file and compute CQT for the first 5 seconds
    y, sr = librosa.load(filename, sr=None, mono=True, duration=5)
    cqt_db = calc_cqt(y, sr, hop_length, n_bins, mag_exp)
    thresholded_cqt = cqt_thresholded(cqt_db)
    
    # Transpose CQT to match the expected input shape (time_steps, n_bins)
    thresholded_cqt = thresholded_cqt.T
    
    # Ensure thresholded_cqt shape matches model input shape (None, n_bins)
    if thresholded_cqt.shape[0] > 431:
        thresholded_cqt = thresholded_cqt[:431, :]
    else:
        padding = 431 - thresholded_cqt.shape[0]
        thresholded_cqt = np.pad(thresholded_cqt, ((0, padding), (0, 0)), 'constant', constant_values=-120)
    
    # Convert lists to arrays for single file processing
    thresholded_cqt = np.expand_dims(thresholded_cqt, axis=0)
    
    # Predict on the audio file
    predictions = model.predict(thresholded_cqt)

    # Estimate tempo for the new audio file
    tempo, _ = librosa.beat.beat_track(y=y, sr=sr)

    # Convert predictions to a MIDI file
    midi = MIDIFile(1)
    midi.addTempo(0, 0, tempo)

    # Add notes to the MIDI file based on the predictions
    for i, pitch in enumerate(predictions[0]):
        if np.max(pitch) > 0.5:  
            midi.addNote(0, 0, int(np.argmax(pitch)), i * hop_length / sr, 1, 100)

    # Save the MIDI file
    output_midi_path = "output.mid"
    with open(output_midi_path, "wb") as output_file:
        midi.writeFile(output_file)

    # Load and parse the MIDI file
    midi_data = MidiFile(output_midi_path)

    # Extract notes information
    notes = []
    for track in midi_data.instruments:
        for note in track.notes:
            notes.append((note.start, note.end, note.pitch, note.velocity))

    # Print notes information
    print("Notes Transcribed:")
    for note in notes:
        print(f"Note: {note[2]}, Start: {note[0]}, End: {note[1]}, Velocity: {note[3]}")
    
    # Set the path to MuseScore executable
    musescore_path = '/Applications/MuseScore 4.app/Contents/MacOS/mscore'
    m21.environment.set('musicxmlPath', musescore_path)

    # Load the MIDI file into music21
    midi_score = m21.converter.parse(output_midi_path)

    # Convert MIDI to MusicXML
    musicxml_path = "output.musicxml"
    midi_score.write('musicxml', fp=musicxml_path)

    # Open the MusicXML file in MuseScore
    subprocess.run([musescore_path, musicxml_path])
    
except Exception as e:
    print(f"Error processing file {filename}: {e}")







'''import os
import librosa
import numpy as np
import matplotlib.pyplot as plt
import librosa.display
from tensorflow.keras.models import Sequential, load_model
from tensorflow.keras.layers import LSTM, Bidirectional, Dense, Dropout, Activation, TimeDistributed, Input
from tensorflow.keras.callbacks import ModelCheckpoint, EarlyStopping, History
from midiutil import MIDIFile
from miditoolkit import MidiFile
import subprocess
import music21 as m21

# Constants for CQT calculation
hop_length = 512
n_bins = 84
mag_exp = 1
cqt_threshold = -30
num_classes = 88  # Adjust based on your task
num_epochs = 25  # Reduced number of epochs
batch_size = 50  # Batch size

# Function to calculate CQT
def calc_cqt(x, fs, hop_length, n_bins, mag_exp):
    c = librosa.cqt(x, sr=fs, hop_length=hop_length, n_bins=n_bins)
    c_mag = np.abs(c) ** mag_exp
    CdB = librosa.amplitude_to_db(c_mag, ref=np.max)
    return CdB

# Function to threshold CQT
def cqt_thresholded(cqt, thres=cqt_threshold):
    new_cqt = np.copy(cqt)
    new_cqt[new_cqt < thres] = -120
    return new_cqt

# Function to generate labels based on pitch detection
def generate_labels(pitches, magnitudes, sr, hop_length, num_classes):
    num_frames = pitches.shape[1]
    labels = np.zeros((num_frames, num_classes))
    
    threshold_value = np.median(magnitudes)  # Adjust as needed
    
    for t in range(num_frames):
        pitch_col = pitches[:, t]
        mag_col = magnitudes[:, t]
        
        active_pitches = np.where(mag_col > threshold_value)[0]
        
        for pitch_idx in active_pitches:
            pitch_value = pitch_col[pitch_idx]
            if pitch_value > 0:  # Only consider non-zero pitches
                pitch_class = min(int(pitch_value), num_classes - 1)
                labels[t, pitch_class] = 1
    
    return labels

# Load your pre-trained model
model = load_model("final_model.keras")
history = History()

# File to train and predict on
filename = "/Users/ramaniatluri/music_transcription-2/audio_music_Guns%20N%20Roses-Sweet%20Child%20O%20Mine%20Intro.mp3"

try:
    print(f"Processing file: {filename}")
    
    # Load audio file and compute CQT for the first 5 seconds
    y, sr = librosa.load(filename, sr=None, mono=True, duration=5)
    cqt_db = calc_cqt(y, sr, hop_length, n_bins, mag_exp)
    thresholded_cqt = cqt_thresholded(cqt_db)
    
    # Transpose CQT to match the expected input shape (time_steps, n_bins)
    thresholded_cqt = thresholded_cqt.T
    
    # Ensure thresholded_cqt shape matches model input shape (None, n_bins)
    if thresholded_cqt.shape[0] > 431:
        thresholded_cqt = thresholded_cqt[:431, :]
    else:
        padding = 431 - thresholded_cqt.shape[0]
        thresholded_cqt = np.pad(thresholded_cqt, ((0, padding), (0, 0)), 'constant', constant_values=-120)
    
    # Pitch estimation for the first 5 seconds
    pitches, magnitudes = librosa.core.piptrack(y=y, sr=sr, hop_length=hop_length)
    
    labels = generate_labels(pitches, magnitudes, sr, hop_length, num_classes)
    
    if labels.shape[0] > 431:
        labels = labels[:431, :]
    else:
        padding = 431 - labels.shape[0]
        labels = np.pad(labels, ((0, padding), (0, 0)), 'constant')
    
    # Convert lists to arrays for single file processing
    thresholded_cqt = np.expand_dims(thresholded_cqt, axis=0)
    labels = np.expand_dims(labels, axis=0)
    
    # Train the model on the single file data
    model.fit(thresholded_cqt, labels, epochs=num_epochs, batch_size=batch_size, callbacks=[history])
    
    # Save the trained model after processing the file
    model.save("final_model_with_single_file.keras")  # Save in Keras format

    print("Training complete!")
    
    # Predict on the same audio file
    X_new = calc_cqt(y, sr, hop_length, n_bins, mag_exp).T  
    if X_new.shape[0] > 431:
        X_new = X_new[:431, :]
    else:
        padding = 431 - X_new.shape[0]
        X_new = np.pad(X_new, ((0, padding), (0, 0)), 'constant', constant_values=-120)
    X_new = np.expand_dims(X_new, axis=0)

    predictions = model.predict(X_new)

    # Estimate tempo for the new audio file
    tempo, _ = librosa.beat.beat_track(y=y, sr=sr)

    # Convert predictions to a MIDI file
    midi = MIDIFile(1)
    midi.addTempo(0, 0, tempo)

    # Add notes to the MIDI file based on the predictions
    for i, pitch in enumerate(predictions[0]):
        if np.max(pitch) > 0.5:  
            midi.addNote(0, 0, int(np.argmax(pitch)), i * hop_length / sr, 1, 100)

    # Save the MIDI file
    output_midi_path = "output.mid"
    with open(output_midi_path, "wb") as output_file:
        midi.writeFile(output_file)

    # Load and parse the MIDI file
    midi_data = MidiFile(output_midi_path)

    # Extract notes information
    notes = []
    for track in midi_data.instruments:
        for note in track.notes:
            notes.append((note.start, note.end, note.pitch, note.velocity))

    # Print notes information
    print("Notes Transcribed:")
    for note in notes:
        print(f"Note: {note[2]}, Start: {note[0]}, End: {note[1]}, Velocity: {note[3]}")
    
    # Set the path to MuseScore executable
    musescore_path = '/Applications/MuseScore 4.app/Contents/MacOS/mscore'
    m21.environment.set('musicxmlPath', musescore_path)

    # Load the MIDI file into music21
    midi_score = m21.converter.parse(output_midi_path)

    # Convert MIDI to MusicXML
    musicxml_path = "output.musicxml"
    midi_score.write('musicxml', fp=musicxml_path)

    # Open the MusicXML file in MuseScore
    subprocess.run([musescore_path, musicxml_path])
    
except Exception as e:
    print(f"Error processing file {filename}: {e}")
'''



'''

import os
import librosa
import numpy as np
import matplotlib.pyplot as plt
import librosa.display
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import LSTM, Bidirectional, Dense, Dropout, Activation, TimeDistributed, Input
from tensorflow.keras.callbacks import ModelCheckpoint, EarlyStopping, History
from midiutil import MIDIFile
from miditoolkit import MidiFile
import subprocess
import music21 as m21

# Constants for CQT calculation
hop_length = 512
n_bins = 84
mag_exp = 1
cqt_threshold = -30
num_classes = 88  # Adjust based on your task
num_epochs = 10  # Reduced number of epochs
batch_size = 50  # Batch size

# Function to calculate CQT
def calc_cqt(x, fs, hop_length, n_bins, mag_exp):
    c = librosa.cqt(x, sr=fs, hop_length=hop_length, n_bins=n_bins)
    c_mag = np.abs(c) ** mag_exp
    CdB = librosa.amplitude_to_db(c_mag, ref=np.max)
    return CdB

# Function to threshold CQT
def cqt_thresholded(cqt, thres=cqt_threshold):
    new_cqt = np.copy(cqt)
    new_cqt[new_cqt < thres] = -120
    return new_cqt

# Function to generate labels based on pitch detection
def generate_labels(pitches, magnitudes, sr, hop_length, num_classes):
    num_frames = pitches.shape[1]
    labels = np.zeros((num_frames, num_classes))
    
    threshold_value = np.median(magnitudes)  # Adjust as needed
    
    for t in range(num_frames):
        pitch_col = pitches[:, t]
        mag_col = magnitudes[:, t]
        
        active_pitches = np.where(mag_col > threshold_value)[0]
        
        for pitch_idx in active_pitches:
            pitch_value = pitch_col[pitch_idx]
            if pitch_value > 0:  # Only consider non-zero pitches
                pitch_class = min(int(pitch_value), num_classes - 1)
                labels[t, pitch_class] = 1
    
    return labels

# Initialize model and history
model = Sequential()
history = History()

# Add Input Layer
model.add(Input(shape=(None, n_bins)))  # Variable-length time dimension
model.add(Bidirectional(LSTM(units=500, return_sequences=True)))

# Add two BiLSTM hidden layers
for _ in range(1):
    model.add(Bidirectional(LSTM(500, return_sequences=True, activation='tanh')))
    model.add(Dropout(0.25))

# Add classification layer to keep learning from the previous audio file and updating 
model.add(TimeDistributed(Dense(num_classes)))
model.add(Activation('sigmoid'))

# Compile model
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['binary_crossentropy'])

# Directory containing audio files
directory_path = '/Users/ramaniatluri/Desktop/audio_file'

# List all mp3 files in the directory
audio_files = [os.path.join(directory_path, file) for file in os.listdir(directory_path) if file.endswith('.mp3')]

# Process the first 50 files
batch_thresholded_cqt = []
batch_y_train = []

# Process the audio files one by one and update the model incrementally
for filename in audio_files[:10]:
    try:
        print(f"Processing file: {filename}")
        
        # Load audio file and compute CQT for the first 5 seconds
        y, sr = librosa.load(filename, sr=None, mono=True, duration=5)
        cqt_db = calc_cqt(y, sr, hop_length, n_bins, mag_exp)
        thresholded_cqt = cqt_thresholded(cqt_db)
        
        # Transpose CQT to match the expected input shape (time_steps, n_bins)
        thresholded_cqt = thresholded_cqt.T
        
        # Ensure thresholded_cqt shape matches model input shape (None, n_bins)
        if thresholded_cqt.shape[0] > 431:
            thresholded_cqt = thresholded_cqt[:431, :]
        else:
            padding = 431 - thresholded_cqt.shape[0]
            thresholded_cqt = np.pad(thresholded_cqt, ((0, padding), (0, 0)), 'constant', constant_values=-120)
        
        # Pitch estimation for the first 5 seconds
        pitches, magnitudes = librosa.core.piptrack(y=y, sr=sr, hop_length=hop_length)
        
        labels = generate_labels(pitches, magnitudes, sr, hop_length, num_classes)
        
        if labels.shape[0] > 431:
            labels = labels[:431, :]
        else:
            padding = 431 - labels.shape[0]
            labels = np.pad(labels, ((0, padding), (0, 0)), 'constant')
        
        # Convert lists to arrays for single file processing
        thresholded_cqt = np.expand_dims(thresholded_cqt, axis=0)
        labels = np.expand_dims(labels, axis=0)
        
        # Train the model on the single file data
        model.fit(thresholded_cqt, labels, epochs=num_epochs, batch_size=batch_size, callbacks=[history])
    
    except Exception as e:
        print(f"Error processing file {filename}: {e}")

# Save the trained model after processing all files
model.save("final_model.keras")  # Save in Keras format

print("Training complete!")'''

'''
# Predict on a new audio file
# You can change 'filename' to the path of a new audio file you want to test
filename = "/Users/ramaniatluri/music_transcription-2/audio_music_Guns%20N%20Roses-Sweet%20Child%20O%20Mine%20Intro.mp3"

try:
    y, sr = librosa.load(filename, sr=None, mono=True, duration=5)
    X_new = calc_cqt(y, sr, hop_length, n_bins, mag_exp).T  
    if X_new.shape[0] > 431:
        X_new = X_new[:431, :]
    else:
        padding = 431 - X_new.shape[0]
        X_new = np.pad(X_new, ((0, padding), (0, 0)), 'constant', constant_values=-120)
    X_new = np.expand_dims(X_new, axis=0)

    predictions = model.predict(X_new)

    # Estimate tempo for the new audio file
    tempo, _ = librosa.beat.beat_track(y=y, sr=sr)

    # Convert predictions to a MIDI file
    midi = MIDIFile(1)
    midi.addTempo(0, 0, tempo)

    # Add notes to the MIDI file based on the predictions
    for i, pitch in enumerate(predictions[0]):
        if np.max(pitch) > 0.5:  
            midi.addNote(0, 0, int(np.argmax(pitch)), i * hop_length / sr, 1, 100)

    # Save the MIDI file
    output_midi_path = "output.mid"
    with open(output_midi_path, "wb") as output_file:
        midi.writeFile(output_file)

    # Load and parse the MIDI file
    midi_data = MidiFile(output_midi_path)

    # Extract notes information
    notes = []
    for track in midi_data.instruments:
        for note in track.notes:
            notes.append((note.start, note.end, note.pitch, note.velocity))

    # Print notes information
    print("Notes Transcribed:")
    for note in notes:
        print(f"Note: {note[2]}, Start: {note[0]}, End: {note[1]}, Velocity: {note[3]}")
    
    # Set the path to MuseScore executable
    musescore_path = '/Applications/MuseScore 4.app/Contents/MacOS/mscore'
    m21.environment.set('musicxmlPath', musescore_path)

    # Load the MIDI file into music21
    midi_score = m21.converter.parse(output_midi_path)

    # Convert MIDI to MusicXML
    musicxml_path = "output.musicxml"
    midi_score.write('musicxml', fp=musicxml_path)

    # Open the MusicXML file in MuseScore
    subprocess.run([musescore_path, musicxml_path])
    
except Exception as e:
    print(f"Error processing file {filename}: {e}")



'''















'''import os
import librosa
import numpy as np
import matplotlib.pyplot as plt
import librosa.display
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import LSTM, Bidirectional, Dense, Dropout, Activation, TimeDistributed, Input
from tensorflow.keras.callbacks import History

# Constants for CQT calculation
hop_length = 512
n_bins = 84
mag_exp = 1
cqt_threshold = -30
num_classes = 88  # Adjust based on your task
num_epochs = 200
batch_size = 40

# Function to calculate CQT
def calc_cqt(x, fs, hop_length, n_bins, mag_exp):
    c = librosa.cqt(x, sr=fs, hop_length=hop_length, n_bins=n_bins)
    c_mag = np.abs(c) ** mag_exp
    CdB = librosa.amplitude_to_db(c_mag, ref=np.max)
    return CdB

# Function to threshold CQT
def cqt_thresholded(cqt, thres=cqt_threshold):
    new_cqt = np.copy(cqt)
    new_cqt[new_cqt < thres] = -120
    return new_cqt

# Function to generate labels based on pitch detection
def generate_labels(pitches, magnitudes, sr, hop_length, num_classes):
    num_frames = pitches.shape[1]
    labels = np.zeros((num_frames, num_classes))
    
    # Determine threshold based on the median magnitude across all pitches
    threshold_value = np.median(magnitudes)  # Adjust as needed
    
    for t in range(num_frames):
        pitch_col = pitches[:, t]
        mag_col = magnitudes[:, t]
        
        # Determine active pitches based on magnitude threshold
        active_pitches = np.where(mag_col > threshold_value)[0]
        
        # Assign labels for active pitches
        for pitch_idx in active_pitches:
            pitch_value = pitch_col[pitch_idx]
            if pitch_value > 0:  # Only consider non-zero pitches
                pitch_class = min(int(pitch_value), num_classes - 1)  # Map pitch to the range of num_classes
                labels[t, pitch_class] = 1
    
    return labels

# Initialize model and history
model = Sequential()
history = History()

# Add Input Layer
model.add(Input(shape=(None, n_bins)))
model.add(Bidirectional(LSTM(units=500, return_sequences=True)))

# Add two BiLSTM hidden layers
for _ in range(1):
    model.add(Bidirectional(LSTM(500, return_sequences=True, activation='tanh')))
    model.add(Dropout(0.25))

# Add classification layer
model.add(TimeDistributed(Dense(num_classes)))
model.add(Activation('sigmoid'))

# Compile model
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['binary_crossentropy'])

# Directory containing audio files
directory_path = '/Users/ramaniatluri/Desktop/audio_file'  #update this one according to ur directory

# List all mp3 files in the directory
audio_files = [os.path.join(directory_path, file) for file in os.listdir(directory_path) if file.endswith('.mp3')]

batch_thresholded_cqt = []
batch_y_train = []

# Training loop
for filename in audio_files:
    print(f"Processing file: {filename}")
    
    # Load audio file and compute CQT for the first 5 seconds
    y, sr = librosa.load(filename, sr=None, mono=True, duration=5)
    cqt_db = calc_cqt(y, sr, hop_length, n_bins, mag_exp)
    thresholded_cqt = cqt_thresholded(cqt_db)
    
    # Ensure thresholded_cqt shape matches model input shape (431, 84)
    if thresholded_cqt.shape[1] > 431:
        thresholded_cqt = thresholded_cqt[:, :431]
    else:
        padding = 431 - thresholded_cqt.shape[1]
        thresholded_cqt = np.pad(thresholded_cqt, ((0, 0), (0, padding)), 'constant', constant_values=-120)
    
    batch_thresholded_cqt.append(thresholded_cqt)
    
    # Pitch estimation for the first 5 seconds
    pitches, magnitudes = librosa.core.piptrack(y=y, sr=sr, hop_length=hop_length)
    
    # Generate labels for the first 5 seconds of the audio file
    labels = generate_labels(pitches, magnitudes, sr, hop_length, num_classes)
    
    if labels.shape[0] > 431:
        labels = labels[:431, :]
    else:
        padding = 431 - labels.shape[0]
        labels = np.pad(labels, ((0, padding), (0, 0)), 'constant')
    
    batch_y_train.append(labels)

# Convert lists to arrays for batch processing
batch_thresholded_cqt = np.array(batch_thresholded_cqt)
batch_y_train = np.array(batch_y_train)

# Train the model on the batched data
model.fit(batch_thresholded_cqt, batch_y_train, epochs=num_epochs, batch_size=batch_size, callbacks=[history])

# Save the trained model
model.save("final_model.h5")

print("Training complete!")'''

'''
import librosa
import numpy as np
from pydub import AudioSegment
from pydub.playback import play
import matplotlib.pyplot as plt
import librosa.display
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import LSTM, Bidirectional, Dense, Dropout, Activation, TimeDistributed, Input
from tensorflow.keras.callbacks import ModelCheckpoint, EarlyStopping, History
from midiutil import MIDIFile
from miditoolkit import MidiFile
import subprocess
import music21 as m21

# Load the audio file
filename = "audio_music_Guns%20N%20Roses-Sweet%20Child%20O%20Mine%20Intro.mp3"
y, sr = librosa.load(filename, sr=None, mono=True, duration=4)

# Constants for CQT calculation
hop_length = 512
n_bins = 84
mag_exp = 1
cqt_threshold = -30

# Function to calculate CQT
def calc_cqt(x, fs, hop_length, n_bins, mag_exp):
    c = librosa.cqt(x, sr=fs, hop_length=hop_length, n_bins=n_bins)
    c_mag = np.abs(c) ** mag_exp
    CdB = librosa.amplitude_to_db(c_mag, ref=np.max)
    return CdB

# Function to threshold CQT
def cqt_thresholded(cqt, thres=cqt_threshold):
    new_cqt = np.copy(cqt)
    new_cqt[new_cqt < thres] = -120
    return new_cqt

# Calculate CQT and threshold it
cqt_db = calc_cqt(y, sr, hop_length, n_bins, mag_exp)
thresholded_cqt = cqt_thresholded(cqt_db)

# Display audio information
print("y shape= ", y.shape)
print("sample rate sr= ", sr)
print("audio length in seconds= ", len(y) / sr)

# Convert audio data to pydub's AudioSegment format
audio_segment = AudioSegment(
    data=y.tobytes(),
    sample_width=y.dtype.itemsize,
    frame_rate=sr,
    channels=1
)

play(audio_segment)

# Tempo estimation
tempo, _ = librosa.beat.beat_track(y=y, sr=sr)
print(f"Estimated Tempo: {tempo} BPM")

# Pitch estimation
pitches, magnitudes = librosa.core.piptrack(y=y, sr=sr, hop_length=hop_length)
pitches = [pitch for pitch in pitches if pitch.any()]
magnitudes = [magnitude for magnitude in magnitudes if magnitude.any()]
pitches = np.array(pitches)
magnitudes = np.array(magnitudes)
print("Pitches: ", pitches)

# Visualize pitches and magnitudes
plt.figure(figsize=(10, 4))
plt.subplot(2, 1, 1)
plt.plot(pitches.T)
plt.title('Estimated pitches over time')
plt.xlabel('Time frames')
plt.ylabel('Pitch')

plt.subplot(2, 1, 2)
plt.plot(magnitudes.T)
plt.title('Magnitude of pitches over time')
plt.xlabel('Time frames')
plt.ylabel('Magnitude')
plt.tight_layout()
plt.show()

# Display the CQT spectrogram
plt.figure(figsize=(10, 4))
librosa.display.specshow(thresholded_cqt, sr=sr, x_axis='time', y_axis='cqt_note')
plt.colorbar(format='%+2.0f dB')
plt.title('Constant-Q power spectrogram')
plt.tight_layout()
plt.show()

# Onset detection
onset_env = librosa.onset.onset_strength(y=y, sr=sr, hop_length=hop_length)
onsets = librosa.onset.onset_detect(onset_envelope=onset_env, sr=sr, hop_length=hop_length)
onset_times = librosa.frames_to_time(onsets, sr=sr, hop_length=hop_length)
print("Onset times: ", onset_times)

# Function to generate labels based on pitch detection
def generate_labels(pitches, magnitudes, sr, hop_length, num_classes):
    num_frames = pitches.shape[1]
    labels = np.zeros((1, num_frames, num_classes))
    
    # Determine threshold based on the median magnitude across all pitches
    threshold_value = np.median(magnitudes)  # Adjust as needed
    
    for t in range(num_frames):
        pitch_col = pitches[:, t]
        mag_col = magnitudes[:, t]
        
        # Determine active pitches based on magnitude threshold
        active_pitches = np.where(mag_col > threshold_value)[0]
        
        # Assign labels for active pitches
        for pitch_idx in active_pitches:
            pitch_value = pitch_col[pitch_idx]
            if pitch_value > 0:  # Only consider non-zero pitches
                pitch_class = min(int(pitch_value), num_classes - 1)  # Map pitch to the range of num_classes
                labels[0, t, pitch_class] = 1
    
    return labels

# Generate labels for training and validation
num_classes = 88  # Adjust based on your task
y_train = generate_labels(pitches, magnitudes, sr, hop_length, num_classes)
y_val = generate_labels(pitches, magnitudes, sr, hop_length, num_classes)

# Preparing data for model training
X = cqt_db.T
X = np.expand_dims(X, axis=0)

# Define the BLSTM model
number_units = 500
number_layers = 2
num_epochs = 200
batch_size = 40

print('Build model...')
model = Sequential()
history = History()

# Add Input Layer
model.add(Input(shape=(None, n_bins)))
model.add(Bidirectional(LSTM(units=number_units, return_sequences=True)))

# Add two BiLSTM hidden layers
for i in range(number_layers - 1):
    print(f"Adding {i+2}th layer of {number_units} units")
    model.add(Bidirectional(LSTM(number_units, return_sequences=True, activation='tanh')))
    model.add(Dropout(0.25))

# Add classification layer
print("Adding classification layer")
model.add(TimeDistributed(Dense(num_classes)))
model.add(Activation('sigmoid'))

# Compile model
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['binary_crossentropy'])

# Callbacks for saving the best model and early stopping
checkpoint = ModelCheckpoint("best_model.keras", monitor='val_loss', save_best_only=True, mode='min')
early_stopping = EarlyStopping(monitor='val_loss', patience=10, mode='min')
callbacks = [checkpoint, early_stopping, history]

# Train the model
model.fit(X, y_train, epochs=num_epochs, batch_size=batch_size, validation_data=(X, y_val), callbacks=callbacks)

# Predict on new audio features
X_new = calc_cqt(y, sr, hop_length, n_bins, mag_exp).T  
X_new = np.expand_dims(X_new, axis=0)
predictions = model.predict(X_new)

# Convert predictions to a MIDI file
midi = MIDIFile(1)  # Create a single track MIDI file
midi.addTempo(0, 0, tempo)

# Add notes to the MIDI file based on the predictions
for i, pitch in enumerate(predictions[0]):
    if np.max(pitch) > 0.5:  
        midi.addNote(0, 0, int(np.argmax(pitch)), i * hop_length / sr, 1, 100)

# Save the MIDI file
output_midi_path = "output.mid"
with open(output_midi_path, "wb") as output_file:
    midi.writeFile(output_file)

# Load and parse the MIDI file
midi_data = MidiFile(output_midi_path)

# Extract notes information
notes = []
for track in midi_data.instruments:
    for note in track.notes:
        notes.append((note.start, note.end, note.pitch, note.velocity))

# Print notes information
print("Notes Transcribed:")
for note in notes:
    print(f"Note: {note[2]}, Start: {note[0]}, End: {note[1]}, Velocity: {note[3]}")

# Set the path to MuseScore executable
musescore_path = '/Applications/MuseScore 4.app/Contents/MacOS/mscore'  # Update this path as needed
m21.environment.set('musicxmlPath', musescore_path)

# Load the MIDI file into music21
midi_score = m21.converter.parse(output_midi_path)

# Convert MIDI to MusicXML
musicxml_path = "output.musicxml"
midi_score.write('musicxml', fp=musicxml_path)

# Open the MusicXML file in MuseScore
subprocess.run([musescore_path, musicxml_path])
'''