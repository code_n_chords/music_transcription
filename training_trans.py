import os
import librosa
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader, TensorDataset
from tqdm import tqdm
import math

# Define your model, loss function, and optimizer
class EmbeddingLayer(nn.Module):
    def __init__(self, d_model, input_dim, max_len=5000):
        super(EmbeddingLayer, self).__init__()

        self.embedding = nn.Embedding(num_embeddings=input_dim, embedding_dim=d_model)
        self.input_size = d_model

        position = torch.arange(0, max_len).unsqueeze(1)
        div_term = torch.exp(torch.arange(0, d_model, 2) * -(math.log(10000.0) / d_model))
        pe = torch.zeros(max_len, d_model)
        pe[:, 0::2] = torch.sin(position * div_term)
        pe[:, 1::2] = torch.cos(position * div_term)
        pe = pe.unsqueeze(0).transpose(0, 1)
        self.register_buffer('pe', pe)

    def forward(self, x):
        embeddings = self.embedding(x) * math.sqrt(self.input_size)
        embeddings = embeddings + self.pe[:x.size(1), :]
        return embeddings

class TransformerModel(nn.Module):
    def __init__(self, input_dim, output_dim, d_model, nhead, num_encoder_layers, num_decoder_layers, dim_feedforward, dropout=0.1):
        super(TransformerModel, self).__init__()
        self.model_type = 'Transformer'
        
        self.embedding = EmbeddingLayer(d_model, input_dim)
        self.transformer = nn.Transformer(d_model, nhead, num_encoder_layers, num_decoder_layers, dim_feedforward, dropout)
        self.fc_out = nn.Linear(d_model, output_dim)
        self.dropout = nn.Dropout(dropout)
        self.sigmoid = nn.Sigmoid()

    def forward(self, src, tgt):
        encoder_input = self.embedding(src)
        decoder_input = self.embedding(tgt)

        src_mask = None
        tgt_mask = self.generate_square_subsequent_mask(tgt.size(1)).to(src.device)

        output = self.transformer(encoder_input, decoder_input, src_mask=src_mask, tgt_mask=tgt_mask)
        output = self.fc_out(self.dropout(output))
        output = self.sigmoid(output)
        return output

    def generate_square_subsequent_mask(self, size):
        mask = (torch.triu(torch.ones(size, size)) == 1).float()
        mask = mask.transpose(0, 1)
        mask = mask.masked_fill(mask == 0, float('-inf'))
        mask = mask.masked_fill(mask == 1, float(0.0))
        return mask

# Hyperparameters
input_dim = 88
output_dim = 88
d_model = 512
nhead = 8
num_encoder_layers = 6
num_decoder_layers = 6
dim_feedforward = 2048
dropout = 0.1
num_epochs = 5
batch_size = 40

# Initialize model, criterion, and optimizer
model = TransformerModel(input_dim, output_dim, d_model, nhead, num_encoder_layers, num_decoder_layers, dim_feedforward, dropout)
criterion = nn.BCELoss()
optimizer = optim.Adam(model.parameters(), lr=0.001)

# Directory containing audio files
directory_path = '/Users/ramaniatluri/Desktop/audio_file'
audio_files = [os.path.join(directory_path, file) for file in os.listdir(directory_path) if file.endswith('.mp3')]

# Process and train
for filename in tqdm(audio_files[:21]):  # Process the first 21 files
    try:
        print(f"Processing file: {filename}")

        # Load audio file and compute CQT for the first 5 seconds
        y, sr = librosa.load(filename, sr=None, mono=True, duration=5)
        cqt_db = calc_cqt(y, sr, hop_length, n_bins, mag_exp)
        thresholded_cqt = cqt_thresholded(cqt_db)

        # Normalize the CQT
        thresholded_cqt = (thresholded_cqt - np.min(thresholded_cqt)) / (np.max(thresholded_cqt) - np.min(thresholded_cqt))

        # Transpose CQT to match the expected input shape (time_steps, n_bins)
        thresholded_cqt = thresholded_cqt.T

        # Ensure thresholded_cqt shape matches model input shape (431, n_bins)
        if thresholded_cqt.shape[0] > 431:
            thresholded_cqt = thresholded_cqt[:431, :]
        else:
            padding = 431 - thresholded_cqt.shape[0]
            thresholded_cqt = np.pad(thresholded_cqt, ((0, padding), (0, 0)), 'constant', constant_values=-120)

        # Pitch estimation for the first 5 seconds
        pitches, magnitudes = librosa.core.piptrack(y=y, sr=sr, hop_length=hop_length)

        labels = generate_labels(pitches, magnitudes, sr, hop_length, num_classes)

        if labels.shape[0] > 431:
            labels = labels[:431, :]
        else:
            padding = 431 - labels.shape[0]
            labels = np.pad(labels, ((0, padding)), 'constant')

        # Convert to PyTorch tensors
        thresholded_cqt = np.expand_dims(thresholded_cqt, axis=0)
        labels = np.expand_dims(labels, axis=0)

        X_train = torch.tensor(thresholded_cqt, dtype=torch.float32)
        y_train = torch.tensor(labels, dtype=torch.float32)

        # Create a PyTorch dataset and dataloader
        train_dataset = TensorDataset(X_train, y_train)
        train_loader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True)

        # Training
        model.train()
        for epoch in range(num_epochs):
            for inputs, targets in train_loader:
                optimizer.zero_grad()
                outputs = model(inputs, inputs)  # Using inputs as both source and target for simplicity
                loss = criterion(outputs, targets)
                loss.backward()
                optimizer.step()

        # Save checkpoint after each file
        torch.save(model.state_dict(), 'model_checkpoint.pth')

    except Exception as e:
        print(f"Error processing file {filename}: {e}")

# Save the final trained model
torch.save(model.state_dict(), 'final_model.pth')



 