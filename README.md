# music_transcription

Overview

The Automatic Music Transcription System is a project designed to convert audio recordings into readable and editable musical notations. The system processes audio clips and generates corresponding sheet music, enabling musicians to interpret, edit, and analyze musical compositions seamlessly.

This project utilizes advanced signal processing and machine learning techniques to provide accurate transcription of musical audio, bridging the gap between sound and notation.

Features

🎵 Key Features
Audio-to-Notation Conversion:
Transcribes audio recordings into standard musical notation displayed on a score sheet.
Supports polyphonic and monophonic music transcription.
Visual Output:
Displays transcribed music as sheet music using tools like MuseScore or similar software.
Exports music scores in formats like MusicXML or PDF for editing and sharing.
MIDI Export:
Outputs transcription as a MIDI file, enabling playback or further digital manipulation.
Drum Onset Detection (Advanced):
Uses a neural network to detect and predict drum instrument onsets for up to 13 different drum types.
🎛 Additional Features
User-friendly interface to upload audio files and view generated music sheets.
Compatibility with popular audio formats like MP3, WAV, and more.
Handles a variety of musical genres and styles.
Workflow

Audio Processing:
Extract features from audio using libraries like Librosa for spectral analysis.
Machine Learning:
A Bidirectional Long Short-Term Memory (BLSTM) neural network is used for detecting onsets and predicting musical notes.
Notation Generation:
Leverages Music21 to process predicted notes and generate notations.
Output Formats:
Transcribes the notes into visual music scores (MuseScore format).
Exports MIDI files for playback.
Technologies Used

Python Libraries:
Librosa: Audio feature extraction.
Music21: Musicological analysis and score generation.
MIDIUtil: MIDI file generation.
Machine Learning:
TensorFlow/Keras: For training and deploying the BLSTM model.
Visualization Tools:
MuseScore: For displaying and editing the musical notation.
